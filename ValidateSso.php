<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

use App\Models\SsoCache;
use App\Models\User;

class ValidateSso
{

    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Authorization');
        $token = str_replace('Bearer', '', $token);
        $token = trim($token);
        if (empty($token)) {
            return response()->json(['message' => 'Authorization_IS_REQUIRED.'], 400);
        }
        $face_recognition_required = config('sso.face_recognition_required');
        $manager_url = config('sso.manager_url');
        $manager_secret = config('sso.manager_secret');

        $user = null;

        // Check cache
        SsoCache::where('expiration', '<=', time())->delete();
        $cache = SsoCache::with('user')->where('token', $token);
        if ($face_recognition_required) {
            $cache->where('face_verified', true);
        }
        $cache = $cache->first();
        if (!empty($cache)) {
            $cache->expiration += 900;
            $cache->save();

            $user = $cache->user;
            Http::async()->withHeaders(['Authorization' => $manager_secret])->post($manager_url.'/api/refresh', ['token' => $token]);
        } else {
            $face = $request->header('Face-Recognition');
            if ($face_recognition_required && empty($face)) {
                return response()->json(['message' => 'Face-Recognition_IS_REQUIRED.'], 400);
            }

            $face_recognition_request = Http::asForm()->withHeaders(['Authorization' => $manager_secret]);
            if ($face_recognition_required) {
                $face_recognition_request->attach('image', base64_decode($face), 'image.jpg', ['Content-Type' => 'image/jpeg']);
            }
            $response = $face_recognition_request->post($manager_url.'/api/introspect', ['token' => $token, 'face_recognition_required' => $face_recognition_required]);

            if (!$response->successful()) {
                return response()->json(['message' => 'INVALID_TOKEN'], 401);
            }
            $data = $response->json();
            $user = User::find($data['user']['id']);

            if (empty($user)) {
                $user = new User();
                $user->id = $data['user']['id'];
                $user->name = $data['user']['name'];
                $user->nip = $data['user']['nip'];
                $user->nrp = $data['user']['nrp'];
                $user->position = $data['user']['position'];
                $user->satker_code = $data['user']['satker_code'];
                $user->satker_name = $data['user']['satker_name'];
                $user->golpang = $data['user']['golpang'];
                $user->email = $data['user']['email'];
                $user->email_verified_at = $data['user']['email_verified_at'];
                $user->password = Hash::make($data['user']['nip']);
                $user->save();
            }

            $cache = new SsoCache();
            $cache->token = $token;
            $cache->user_id = $user->id;
            $cache->expiration = $data['expiration'];
            if ($face_recognition_required) {
                $cache->face_verified = true;
                $user->face_verified_at = (string) Carbon::now();
                $user->save();
            }
            $cache->save();
        }
        $request->setUserResolver(function() use ($user) {
            return $user;
        });
        Auth::login($user);

        return $next($request);
    }
}
